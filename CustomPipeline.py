from sklearn.preprocessing import OneHotEncoder
import pandas as pd

class LabelBinarizerTransformer():
    def __init__(self):
        pass

    def transform(self,X,y=None):
        oneHotEncoder = OneHotEncoder(sparse_output=False)
        data = oneHotEncoder.fit_transform(X)
        return pd.DataFrame(data=data, columns=data.columns[1::2])
    
    def fit(self, X, y=None):
        return self

class GroupValueTransformer():
    def __init__(self, name, labels, bins):
        self.name = name
        self.labels = labels
        self.bins = bins
        pass

    def transform(self,X,y=None):
        return pd.DataFrame(pd.cut(X[self.name], bins=self.bins, labels=self.labels), columns=[self.name])
    
    def fit(self, X, y=None):
        return self
    
class PassThroughTransformer():
    def __init__(self):
        pass

    def transform(self,X,y=None):
        return X
    
    def fit(self, X, y=None):
        return self

class InOutTransfromer():
    def __init__(self):
        pass

    def transform(self,X,y=None):
        DataToAdd = pd.DataFrame()
        duration = pd.DataFrame()

        in_time = X.filter(regex='_in')
        out_time = X.filter(regex='_out')
        
        for col in in_time.columns[1:]:
            in_col = col
            out_col = col.replace('_in','_out')
            duration = pd.concat([duration, X[out_col] - X[in_col]], axis=1)
            
        mean_in_time = pd.DataFrame()
        std_in_time = pd.DataFrame()
        mean_out_time = pd.DataFrame()
        std_out_time = pd.DataFrame()

        for (index_in, row_in), (index_out, row_out) in zip(in_time[in_time.columns].iterrows(), out_time[out_time.columns].iterrows()):
            row_in = row_in - pd.to_datetime(row_in.dt.strftime('%Y-%m-%d'))
            row_out = row_out - pd.to_datetime(row_out.dt.strftime('%Y-%m-%d'))
            mean_in_time = pd.concat([mean_in_time, pd.Series(row_in.mean())], axis=0, ignore_index=True)
            std_in_time = pd.concat([std_in_time, pd.Series(row_in.std())], axis=0, ignore_index=True)
            mean_out_time = pd.concat([mean_out_time, pd.Series(row_out.mean())], axis=0, ignore_index=True)
            std_out_time = pd.concat([std_out_time, pd.Series(row_out.std())], axis=0, ignore_index=True)
        
        DataToAdd['mean_duration'] = round(duration.mean(axis=1).dt.total_seconds() / 3600, 2)
        DataToAdd['std_duration'] = round(duration.std(axis=1).dt.total_seconds() / 3600, 2)
        DataToAdd['mean_in_time'] = mean_in_time.apply(lambda x: round(x.dt.total_seconds() / 3600, 2))
        DataToAdd['std_in_time'] = std_in_time.apply(lambda x: round(x.dt.total_seconds() / 3600, 2))
        DataToAdd['mean_out_time'] = mean_out_time.apply(lambda x: round(x.dt.total_seconds() / 3600, 2))
        DataToAdd['std_out_time'] = std_out_time.apply(lambda x: round(x.dt.total_seconds() / 3600, 2))
        DataToAdd['day_away'] = duration.isna().sum(axis=1)
        return DataToAdd
    
    def fit(self, X, y=None):
        return self