from sklearn.metrics import f1_score as f1_score_sklearn, roc_curve as roc_curve_sklearn, precision_score, recall_score, precision_recall_curve, roc_auc_score, confusion_matrix
import matplotlib.pyplot as plt
from sklearn.model_selection import cross_val_predict
import numpy as np
import pandas as pd

class DataAnalysis:
    def __init__(self, data_training):
        self.model = data_training.model
        self.X_train = data_training.X_train
        self.y_train = data_training.y_train
        self.X_test = data_training.X_test
        self.y_test = data_training.y_test
        self.data = data_training.data
        self.y_test_predicted = data_training.predict(self.X_test)
        self.y_train_predicted = data_training.predict(self.X_train)

    def correlation_matrix(self, col):
        correlation_matrix_value = self.data.corr()
        # fig = plt.figure(figsize=(8, 8))
        # ax = fig.add_subplot(111)
        # cax = ax.matshow(correlation_matrix_value, vmin=-1, vmax=1)
        # fig.colorbar(cax)
        # ticks = np.arange(0, len(self.X_train.columns), 1)
        # ax.set_xticks(ticks)
        # plt.xticks(rotation=90)
        # ax.set_yticks(ticks)
        # ax.set_xticklabels(self.X_train.columns)
        # ax.set_yticklabels(self.X_train.columns)
        # plt.show()
        corr = correlation_matrix_value[col].sort_values(ascending=False)
        print("Correlation with", col, ":")
        print(corr[1:6])
        print()
        print("Anticorrelation with", col, ":")
        print(corr[-5:])
        print()
        return corr[1:6], corr[-5:]
    
    def confusion_matrix(self):
        confusion_matrix_value = confusion_matrix(self.y_test, self.y_test_predicted)
        # fig = plt.figure(figsize=(8, 8))
        # ax = fig.add_subplot(111)
        # cax = ax.matshow(confusion_matrix_value)
        # fig.colorbar(cax)
        # plt.show()
        print("Confusion matrix:")
        print(confusion_matrix_value)
        print()
        return confusion_matrix_value
    
    def precision(self):
        precision_value = precision_score(self.y_test, self.y_test_predicted, average='macro')
        print("Precision:", precision_value)
        print()
        return precision_value

    def recall(self):
        recall_value = recall_score(self.y_test, self.y_test_predicted, average='macro')
        print("Recall:", recall_value)
        print()
        return recall_value

    def f1_score(self):
        f1_score_value = f1_score_sklearn(self.y_test, self.y_test_predicted, average='macro')
        print("F1 Score:", f1_score_value)
        print()
        return f1_score_value

    def roc_curve(self):
        fpr_test, tpr_test, thresholds = roc_curve_sklearn(self.y_test, self.cross_validation(set="test"))
        fpr_train, tpr_train, thresholds = roc_curve_sklearn(self.y_train, self.cross_validation(set="train"))
        # plt.plot(fpr_test, tpr_test, linewidth=2, label="Test")
        plt.plot(fpr_train, tpr_train, linewidth=2, label="Train")
        plt.plot([0, 1], [0, 1], 'k--')
        plt.axis([0, 1, 0, 1])
        plt.xlabel('False Positive Rate', fontsize=16)
        plt.ylabel('True Positive Rate', fontsize=16)
        plt.legend(loc="lower right", fontsize=14)
        plt.figure(figsize=(8, 6))
        plt.show()

    def roc_auc_score(self):
        roc_auc_score_value = roc_auc_score(self.y_test, self.y_test_predicted)
        print("ROC AUC Score:", roc_auc_score_value)
        print()
        return roc_auc_score_value

    def cross_validation(self, set):
        scores = cross_val_predict(self.model, self.X_test if set=="test" else self.X_train, self.y_test if set=="test" else self.y_train, method="predict_proba")
        return scores[:, 1]
    
    def precision_recall_curve(self):
        precisions, recalls, thresholds = precision_recall_curve(self.y_test, self.y_test_predicted)
        plt.plot(recalls, precisions, "k-", linewidth=2)
        plt.xlabel("Recall", fontsize=16)
        plt.ylabel("Precision", fontsize=16)
        plt.axis([0, 1, 0, 1])
        plt.figure(figsize=(8, 6))
        plt.show()

    def features_importance(self):
        print("Features Importance:")
        all_features = sorted(zip(self.model.feature_importances_, self.X_train.columns), reverse=True)
        print("Top 5 top features:")
        for data in all_features[:5]:
            print(data[1], ":", data[0])
        print("Top 5 least features:")
        for data in all_features[-5:]:
            print(data[1], ":", data[0])
    
    def roc_auc_score(self):
        roc_auc_score_value = roc_auc_score(self.y_test, self.y_test_predicted)
        print("ROC AUC Score:", roc_auc_score_value)
        print()
        return roc_auc_score_value

    def all(self):
        self.confusion_matrix()
        self.precision()
        self.recall()
        self.f1_score()
        self.roc_curve()
        self.roc_auc_score()
        self.features_importance()