from sklearn.model_selection import StratifiedShuffleSplit, RandomizedSearchCV

class DataTraining:
    def __init__(self, data):
        self.data = data
        self.X_train, self.X_test, self.y_train, self.y_test = None, None, None, None
        self.model = None

    def split(self, test_size, label, on):
        stratifiedShuffleSplit = StratifiedShuffleSplit(n_splits=1, test_size=test_size, random_state=42)
        for train_index, test_index in stratifiedShuffleSplit.split(self.data, self.data[on]):
            train, test = self.data.loc[train_index], self.data.loc[test_index]
        self.X_train = train.drop(label, axis=1)
        self.X_test = test.drop(label, axis=1)
        self.y_train = train[label]
        self.y_test = test[label]

        
    def train(self, model):
        self.model = model
        self.model.fit(self.X_train, self.y_train)

    def tune(self, model, param_distribs, n_iter, cv=5):
        model_tuned = RandomizedSearchCV(model, param_distributions=param_distribs,
                                        n_iter=n_iter, cv=cv, scoring='neg_mean_squared_error')
        model_tuned.fit(self.X_train, self.y_train)
        self.model = model_tuned.best_estimator_
    
    def test(self):
        return self.model.score(self.X_test, self.y_test)

    def predict(self, data):
        return self.model.predict(data)
