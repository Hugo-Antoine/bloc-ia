from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OrdinalEncoder, OneHotEncoder, StandardScaler
from CustomPipeline import PassThroughTransformer, LabelBinarizerTransformer, GroupValueTransformer, InOutTransfromer

import sklearn
sklearn.set_config(transform_output="pandas")
import yaml
import pandas as pd

class DataWrangling:
    def __init__(self, dataframe_paths, on, config_path):
        self.raw_data = pd.read_csv(dataframe_paths[0])
        self.prepared_data = None
        with open(config_path, 'r') as stream:
            try:
                self.config = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                raise ValueError("Invalid config file")
        for dataframe_path in dataframe_paths[1:]:
            self.raw_data = self.raw_data.merge(pd.read_csv(dataframe_path), on = on)
    
    def main_pipeline(self):
        transformers = []
        for column_config in self.config:
            if(column_config["steps"][0]["name"] != "DropValue"):
                pipeline = []
                for step in column_config["steps"]:
                    if(step["name"] == "OrdinalEncoder"):
                        pipeline.append(("OrdinalEncoder", OrdinalEncoder(categories=[step["categories"]])))
                    elif(step["name"] == "LabelBinarizer"):
                        pipeline.append(("LabelBinarizer", LabelBinarizerTransformer()))
                    elif(step["name"] == "OneHotEncoder"):
                        pipeline.append(("OneHotEncoder", OneHotEncoder(sparse_output=False)))
                    elif(step["name"] == "StandardScaler"):
                        pipeline.append(("StandardScaler", StandardScaler()))
                    elif(step["name"] == "SimpleImputer"):
                        pipeline.append(("SimpleImputer", SimpleImputer(strategy=step["strategy"])))
                    elif(step["name"] == "GroupValue"):
                        pipeline.append(("GroupValue", GroupValueTransformer(name=column_config["name"], labels=step["labels"], bins=step["bins"])))
                    elif(step["name"] == "PassThrough"):
                        pipeline.append(("PassThrough", PassThroughTransformer()))
                    else:
                        raise ValueError("Unknown step name " + step["name"])
                transformers.append((column_config["name"] + "_pipeline", Pipeline(pipeline), [column_config["name"]]))     
        full_pipeline = ColumnTransformer(
            transformers=transformers, 
            verbose_feature_names_out=False,
        )
        self.prepared_data = full_pipeline.fit_transform(self.raw_data)
        
    def in_out_pipeline(self, in_time_path, out_time_path):
        in_time = pd.read_csv(in_time_path).rename(columns={"Unnamed: 0" :'EmployeeID'})
        in_time[in_time.columns[1:]] = in_time[in_time.columns[1:]].apply(pd.to_datetime, format="%Y-%m-%d %H:%M:%S")
        out_time = pd.read_csv(out_time_path).rename(columns={"Unnamed: 0" :'EmployeeID'})
        out_time[out_time.columns[1:]] = out_time[out_time.columns[1:]].apply(pd.to_datetime, format="%Y-%m-%d %H:%M:%S")
        all_time = in_time.merge(out_time, on = "EmployeeID", suffixes=("_in", "_out"))
        in_out_pipe = Pipeline([
            ("in_out_transformer", InOutTransfromer()),
            ("in_out_scaler", StandardScaler())
        ])
        PassThroughPiepline = Pipeline([
            ("PassThrough", PassThroughTransformer())
        ])
        full_pipeline = ColumnTransformer(
            transformers=[
            ("in_out_pipeline", in_out_pipe, all_time.columns),
            ("in_out_passtrough", PassThroughPiepline, [all_time.columns[0]])
            ], 
            verbose_feature_names_out=False,
        )
        self.prepared_data = self.prepared_data.merge(full_pipeline.fit_transform(all_time), on = "EmployeeID")